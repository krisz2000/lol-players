/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.PlayerService;

/**
 *
 * @author Win10
 */
public class PlayerController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            PlayerService ps = new PlayerService();
            if (request.getParameter("task") != null) {
                if (request.getParameter("task").equals("changeNickname")
                        && request.getParameter("nick") != null
                        && request.getParameter("newNick") != null) {
                    Boolean result = ps.changeNickname(request.getParameter("nick"),
                            request.getParameter("newNick"));
                    out.write(result.toString());
                }

                if (request.getParameter("task").equals("getNameByNo")
                        && request.getParameter("no") != null) {
                    String result = ps.findPlayerNameByNo(Integer.parseInt(request.getParameter("no")));
                    String resultString = "{\"name\":\"" + result + "\"}";
                    out.write(resultString);
                }
                if (request.getParameter("task").equals("start")) {
                    ps.start();
                    out.write("true");
                }
                if (request.getParameter("task").equals("load")) {
                    out.write(ps.findAllPlayers());
                }
                if (request.getParameter("task").equals("editPlayer") && request.getParameter("no") != null){
                    boolean result = ps.editPlayer(request);
                    out.write(String.valueOf(result));
                }
                if (request.getParameter("task").equals("addPlayer")){
                    boolean result = ps.addPlayer(request);
                    out.write(String.valueOf(result));
                }
                if (request.getParameter("task").equals("removePlayer") && request.getParameter("no") != null){
                    boolean result = ps.removePlayer(request);
                    out.write(String.valueOf(result));
                }
            }

        }catch(Exception e){
            System.out.println(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
