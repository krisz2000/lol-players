/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.Team;

public class TeamService {

    public Boolean changeName(String name, String newName) {
        Team t = findTeam(name);
        if (t == null) {
            return Boolean.FALSE;
        }
        t.setName(newName);
        return Boolean.TRUE;
    }

    public Boolean changeOwner(String name, String newOwner) {
        Team t = findTeam(name);
        if (t == null) {
            return Boolean.FALSE;
        }
        t.setOwner(newOwner);
        return Boolean.TRUE;
    }

    public Boolean addSponsor(String name, String sponsor) {
        Team t = findTeam(name);
        if (t == null) {
            return Boolean.FALSE;
        }
        List<String> sponsors = t.getSponsors();
        sponsors.add(sponsor);
        t.setSponsors(sponsors);
        return Boolean.TRUE;
    }

    public Boolean removeSponsor(String name, String sponsor) {
        Team t = findTeam(name);
        if (t == null) {
            return Boolean.FALSE;
        }
        List<String> sponsors = t.getSponsors();
        Boolean removed = sponsors.remove(sponsor);
        if (!removed) {
            return Boolean.FALSE;
        }
        t.setSponsors(sponsors);
        return Boolean.TRUE;
    }

    public Boolean moveLocation(String name, String newLocation) {
        Team t = findTeam(name);
        if (t == null) {
            return Boolean.FALSE;
        }
        t.setLocation(newLocation);
        return Boolean.TRUE;
    }

    public static Team findTeam(String name) {
        try {
            Team t = new Team();
            Boolean found = false;
            for (Team team : Team.teams) {
                if (team.getName().equals(name)) {
                    t = team;
                    found = true;
                    break;
                }
            }
            if (!found) {
                return null;
            }
            return t;
        } catch (Exception ex) {
            System.out.println("Error: " + ex.toString());
            return null;
        }
    }
    
    public Team findTeamByNo(Integer no) {
        try {
            return Team.teams.get(no);
        } catch (Exception ex) {
            System.out.println("Error: " + ex.toString());
            return null;
        }
    }

    public void start() {
        Team t = new Team(Boolean.TRUE);
    }
    
    public String findAllTeams(){
        String json = "{\"teams\":[";
        int number = 0;
        for(Team player : Team.teams){
            json += "{\"number\": \"" + number + "\",";
            json += "\"name\": \"" + player.getName() + "\",";
            json += "\"location\": \"" + player.getLocation()+ "\",";
            json += "\"owner\": \"" + player.getOwner() + "\",";
            json += "\"sponsors\": [";
            List<String> sponsors = player.getSponsors();
            for(int i = 0; i < sponsors.size(); i++){
                if(i == 0){
                    json += ("\"" + sponsors.get(i) + "\"");
                }else{
                    json += (" ,\"" + sponsors.get(i) + "\"");
                }
            }
            json += "]},";
            number++;
        }
        json = json.substring(0, json.length() - 1);
        json += "]}";
        System.out.println(json);
        return json;
    }
    
    public boolean editTeam(HttpServletRequest request){
        try{
        Team p = findTeamByNo(Integer.parseInt(request.getParameter("no")));
        p.setName(request.getParameter("name"));
        p.setLocation(request.getParameter("location"));
        p.setOwner(request.getParameter("owner"));
        p.setSponsors(Arrays.asList(request.getParameter("sponsors").split(",")));
        return true;
        }catch(Exception e){
            return false;
        }
    }
    public boolean addTeam(HttpServletRequest request){
        try{
        Team t = new Team();
        t.setName(request.getParameter("name"));
        t.setLocation(request.getParameter("location"));
        t.setOwner(request.getParameter("owner"));
        t.setSponsors(Arrays.asList(request.getParameter("sponsors").split(",")));
        Team.teams.add(t);
        return true;
        }catch(Exception e){
            return false;
        }
    }
    
    public boolean removeTeam(HttpServletRequest request){
        try{
        Team.teams.remove(Integer.parseInt(request.getParameter("no")));
        return true;
        }catch(Exception e){
            return false;
        }
    }
}
