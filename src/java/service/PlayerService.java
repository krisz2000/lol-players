/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import model.Player;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Win10
 */
public class PlayerService {

    public Boolean changeNickname(String nick, String newNick) {
        Player p = findPlayer(nick);
        if (p == null) {
            return Boolean.FALSE;
        }
        List<String> aliases = p.getAliases();
        if(!aliases.contains(nick)){
            aliases.add(nick);
            p.setAliases(aliases);
        }
        p.setNickname(newNick);
        
        return Boolean.TRUE;
    }

    public Boolean roleSwap(String nick, String newRole) {
        Player p = findPlayer(nick);
        if (p == null) {
            return Boolean.FALSE;
        }
        p.setRole(newRole);
        return Boolean.TRUE;
    }

    public Boolean addFavouriteChamp(String nick, String champ) {
        Player p = findPlayer(nick);
        if (p == null) {
            return Boolean.FALSE;
        }
        List<String> favouriteChamps = p.getFavouriteChamps();
        favouriteChamps.add(champ);
        p.setFavouriteChamps(favouriteChamps);
        return Boolean.TRUE;
    }

    public Boolean removeFavouriteChamp(String nick, String champ) {
        Player p = findPlayer(nick);
        if (p == null) {
            return Boolean.FALSE;
        }
        List<String> favouriteChamps = p.getFavouriteChamps();
        Boolean removed = favouriteChamps.remove(champ);
        if (!removed) {
            return Boolean.FALSE;
        }
        p.setFavouriteChamps(favouriteChamps);
        return Boolean.TRUE;
    }

    public Boolean changeTeam(String nick, String newTeam) {
        Player p = findPlayer(nick);
        if (p == null) {
            return Boolean.FALSE;
        }
        List<String> oldTeams = p.getOldTeams();
        if(!oldTeams.contains(p.getCurrentTeam())){
            oldTeams.add(p.getCurrentTeam());
            p.setOldTeams(oldTeams);
        }
        p.setCurrentTeam(newTeam);
        return Boolean.TRUE;
    }

    public Boolean leaveTeam(String nick) {
        Player p = findPlayer(nick);
        if (p == null) {
            return Boolean.FALSE;
        }
        List<String> oldTeams = p.getOldTeams();
        oldTeams.add(p.getCurrentTeam());
        p.setOldTeams(oldTeams);
        p.setCurrentTeam(null);
        return Boolean.TRUE;
    }

    public Player findPlayer(String nick) {
        try {
            Player p = new Player();
            Boolean found = false;
            for (Player player : Player.players) {
                if (player.getNickname().equals(nick)) {
                    p = player;
                    found = true;
                    break;
                }
            }
            if (!found) {
                return null;
            }
            return p;
        } catch (Exception ex) {
            System.out.println("Error: " + ex.toString());
            return null;
        }
    }

    public String findPlayerNameByNo(Integer no) {
        try {
            return Player.players.get(no).getNickname();
        } catch (Exception ex) {
            System.out.println("Error: " + ex.toString());
            return null;
        }
    }
    
    public Player findPlayerByNo(Integer no) {
        try {
            return Player.players.get(no);
        } catch (Exception ex) {
            System.out.println("Error: " + ex.toString());
            return null;
        }
    }
    
    public void start() {
        Player p = new Player(Boolean.TRUE);
    }
    
    public String findAllPlayers(){
        String json = "{\"players\":[";
        int number = 0;
        for(Player player : Player.players){
            json += "{\"number\": \"" + number + "\",";
            json += "\"nickname\": \"" + player.getNickname() + "\",";
            json += "\"name\": \"" + player.getName() + "\",";
            json += "\"countryOfBirth\": \"" + player.getCountryOfBirth()+ "\",";
            json += "\"role\": \"" + player.getRole() + "\",";
            json += "\"aliases\": [";
            List<String> aliases = player.getAliases();
            for(int i = 0; i < aliases.size(); i++){
                if(i == 0){
                    json += ("\"" + aliases.get(i) + "\"");
                }else{
                    json += (" ,\"" + aliases.get(i) + "\"");
                }
            }
            json += "],";
            json += "\"favouriteChamps\": [";
            List<String> favouriteChamps = player.getFavouriteChamps();
            for(int i = 0; i < favouriteChamps.size(); i++){
                if(i == 0){
                    json += ("\"" + favouriteChamps.get(i) + "\"");
                }else{
                    json += (" ,\"" + favouriteChamps.get(i) + "\"");
                }
            }
            json += "],";
            json += "\"currentTeam\": \"" + player.getCurrentTeam() + "\",";
            json += "\"oldTeams\": [";
            List<String> oldTeams = player.getOldTeams();
            for(int i = 0; i < oldTeams.size(); i++){
                if(i == 0){
                    json += ("\"" + oldTeams.get(i) + "\"");
                }else{
                    json += (" ,\"" + oldTeams.get(i) + "\"");
                }
            }
            json += "]},";
            number++;
        }
        if(json.substring(json.length() - 1).equals(",")){
            json = json.substring(0, json.length() - 1);
        }
        json += "]}";
        System.out.println(json);
        return json;
    }
    
    public boolean editPlayer(HttpServletRequest request){
        try{
        Player p = findPlayerByNo(Integer.parseInt(request.getParameter("no")));
        p.setName(request.getParameter("name"));
        p.setCountryOfBirth(request.getParameter("countryOfBirth"));
        p.setRole(request.getParameter("role"));
        if(!request.getParameter("aliases").equals("")){
            p.setAliases(toList(request.getParameter("aliases").split(",")));
        }
        if(!request.getParameter("favouriteChamps").equals("")){
            p.setFavouriteChamps(toList(request.getParameter("favouriteChamps").split(",")));
        }
        if(!request.getParameter("oldTeams").equals("")){
            p.setOldTeams(toList(request.getParameter("oldTeams").split(",")));
        }
        if(!request.getParameter("nickname").equals(p.getNickname())){
            changeNickname(p.getNickname(), request.getParameter("nickname"));
        }
        if(!request.getParameter("currentTeam").equals(p.getCurrentTeam())){
            changeTeam(p.getNickname(), request.getParameter("currentTeam"));
        }
        return true;
        }catch(Exception e){
            return false;
        }
    }
    public boolean addPlayer(HttpServletRequest request){
        try{
        Player p = new Player();
        p.setNickname(request.getParameter("nickname"));
        p.setName(request.getParameter("name"));
        p.setCountryOfBirth(request.getParameter("countryOfBirth"));
        p.setRole(request.getParameter("role"));
        if(!request.getParameter("aliases").equals("")){
            p.setAliases(toList(request.getParameter("aliases").split(",")));
        }
        if(!request.getParameter("favouriteChamps").equals("")){
            p.setFavouriteChamps(toList(request.getParameter("favouriteChamps").split(",")));
        }
        if(!request.getParameter("oldTeams").equals("")){
            p.setOldTeams(toList(request.getParameter("oldTeams").split(",")));
        }
        p.setCurrentTeam(request.getParameter("currentTeam"));
        Player.players.add(p);
        return true;
        }catch(Exception e){
            return false;
        }
    }
    
    public boolean removePlayer(HttpServletRequest request){
        try{
        Player.players.remove(Integer.parseInt(request.getParameter("no")));
        return true;
        }catch(Exception e){
            return false;
        }
    }
    public List<String> toList(String[] array){
        List<String> list = new ArrayList<>();
        list.addAll(Arrays.asList(array));
        return list;
    }
}
