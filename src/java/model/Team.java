/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Team {

    public static ArrayList<Team> teams = new ArrayList<Team>();

    private String name;
    private String location;
    private String owner;
    private List<String> sponsors;

    public Team(Boolean start) {
        teams.clear();
        teams.add(new Team("Fnatic", "United Kingdom", "Sam Mathews", new ArrayList<>(Arrays.asList("OnePlus", "DXRacer", "OnePlus"))));
        teams.add(new Team("G2", "Germany", "Carlos Rodríguez Santiago", new ArrayList<>(Arrays.asList("Logitech G", "AOC", "NeedforSeat"))));
        teams.add(new Team("T1", "South Korea", "SK Telecom", new ArrayList<>(Arrays.asList("SKT 5GX", "Twitch", "Logitech G"))));
    }

    public Team() {

    }

    public Team(String name, String location, String owner, ArrayList<String> sponsors) {
        this.name = name;
        this.location = location;
        this.owner = owner;
        this.sponsors = sponsors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<String> getSponsors() {
        return sponsors;
    }

    public void setSponsors(List<String> sponsors) {
        this.sponsors = sponsors;
    }

}
