/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import service.TeamService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Player {

    public static ArrayList<Player> players = new ArrayList<Player>();

    private String nickname;
    private String name;
    private String countryOfBirth;
    private String role;
    private List<String> aliases = new ArrayList<>();
    private List<String> favouriteChamps = new ArrayList<>();
    private String currentTeam;
    private List<String> oldTeams = new ArrayList<>();

    public Player(Boolean start) {
        players.clear();
        players.add(new Player("Caps", "Rasmus Borregaard Winther", "Denmark", "Mid", new ArrayList(),
                new ArrayList<>(Arrays.asList("Zoe", "Ryze", "Yasuo")), "G2",
                new ArrayList<>(Arrays.asList("Fnatic"))));
        players.add(new Player("Rekkles", "Martin Larsson", "Sweden", "Adc", new ArrayList(),
                new ArrayList<>(Arrays.asList("Tristana", "Sivir", "Kennen")), "Fnatic",
                new ArrayList()));
        players.add(new Player("Faker", "Lee Sang-hyeok", "South Korea", "Mid", new ArrayList(),
                new ArrayList<>(Arrays.asList("Zed", "LeBlanc", "Ryze")), "T1",
                new ArrayList()));

    }

    public Player() {

    }

    public Player(String nickname, String name, String countryOfBirth, String role, ArrayList<String> aliases, ArrayList<String> favouriteChamps, String currentTeam, ArrayList<String> oldTeams) {
        this.nickname = nickname;
        this.name = name;
        this.countryOfBirth = countryOfBirth;
        this.role = role;
        this.aliases = aliases;
        this.favouriteChamps = favouriteChamps;
        this.currentTeam = currentTeam;
        this.oldTeams = oldTeams;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    public List<String> getFavouriteChamps() {
        return favouriteChamps;
    }

    public void setFavouriteChamps(List<String> favouriteChamps) {
        this.favouriteChamps = favouriteChamps;
    }

    public String getCurrentTeam() {
        return currentTeam;
    }

    public void setCurrentTeam(String currentTeam) {
        this.currentTeam = currentTeam;
    }

    public List<String> getOldTeams() {
        return oldTeams;
    }

    public void setOldTeams(List<String> oldTeams) {
        this.oldTeams = oldTeams;
    }

}
